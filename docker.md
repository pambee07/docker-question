Qu'est-ce qu'une machine virtuelle ?
Une machine virtuelle est un ordinateur simulé qui fonctionne à l'intérieur du véritable ordinateur. Elle permet d'exécuter différents systèmes d'exploitation sur une seule machine physique.

Qu'est-ce que Docker, et quelle est la différence avec une machine virtuelle ?
Docker est une plateforme utilisant des conteneurs légers pour exécuter des applications avec leurs dépendances, codes, bibliothèques, variables d'environnement, fichiers de configuration offrant une efficacité et une portabilité supérieures par rapport aux machines virtuelles qui ont des systèmes d'exploitation complets.

Qu'est-ce qu'une image Docker ?
Une image Docker est un package léger et autonome contenant tout le nécessaire pour exécuter une application, y compris le code, les bibliothèques, les dépendances, et les configurations.

Qu'est-ce qu'un conteneur (container) Docker ?
Un conteneur Docker est une instance exécutable d'une image Docker. Il encapsule une application et ses dépendances, s'exécutant de manière isolée.

Qu'est-ce que Docker Hub ?
Docker Hub est un service cloud public fourni par Docker. Il sert de registre central pour les images Docker, permettant aux utilisateurs de stocker, partager et télécharger des images Docker préconstruites.